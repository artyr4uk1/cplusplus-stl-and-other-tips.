// * Meyers Tips 9
// Be careful with deleting elements from containers

bool suitableValue(int val)
{
    return (!val);
}

bool suitableValue(const std::pair<int, std::string> & spair)
{
    return (spair.first < 3);
}

int main()
{

    std::vector<int> vec;
    for (int i = 0; i < 5; ++i)
        vec.push_back(i);
    vec.push_back(0); // vec: 0 1 2 3 4 0

    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));

    // Good case for removing elements from sequence containers vector/string/deque:
    vec.erase(std::remove(vec.begin(),vec.end(), 0), vec.end()); //
    // also can use std::remove_if with predicate
    // std::list has interface funtction to remove elements direcly(using erase-remove idiom):
    // container.remove(value);
    // container.remove_if(predcate);

    // If we need make some operations with sequence container iterator and then remove it:
    for(vector<int>::iterator it = vec.begin(); it != vec.end();)
    {
        if (suitableValue(*it))
        {
            //some operation with it
            it = vec.erase(it);
        }
        else
            ++it;
    }

    std::map<int, std::string> mapIntStr;
    mapIntStr.insert(std::make_pair<int, string>(0, "apple"));
    mapIntStr.insert(std::make_pair<int, string>(1, "pear"));
    mapIntStr.insert(std::make_pair<int, string>(3, "nut"));


    // How to remove elements from associative container with predicate:
    // 1:

    std::map<int, std::string> goodValues;
    std::remove_copy_if(mapIntStr.begin(), mapIntStr.end(),
                        std::inserter(goodValues, goodValues.end()),suitableValue);

    mapIntStr.swap(goodValues);

    //2:

    for (std::map<int, string>::iterator it = mapIntStr.begin(); it != mapIntStr.end();)
    {
        if (suitableValue(*it))
            mapIntStr.erase(it++);
        else
            ++it;
    }

    return 0;
}

.    // To remove all elements with specific value:
    
    // 1. vector/string/deque - use idiom erase-remove
    // 2. list - list::remove
    // 3. standart associative container - use container::erase

    // To remove all elements that match the predicate:
    
    // 1. vector/string/deque - use idiom erase-remove_if
    // 2. list - list::remove
    // 3. standart 
    
    // Additional operations in loop:
    // 1. associative container - erase(it++);
    // 2. sequence  - it = container.erase(it);
    

// * Meyers Tips 9 *

// * Meyers Tips 26 *

// Prefer using iterator insted of const_iterator/reverse_iterator/const_reverse_iterator cause:
//                  1. Some type of interfaces of insert/erase requires only iterator type.
//                  2. Cast from const_iterator to iterator is worthy and not effective sometimes
//                  3. Cast from reverse_iterator to iterator may require an additional operation for iterators.
// Sometimes realization of iterators define operator functions localy inside the class instead of globally with requering two arguments. 
// That can cause the compilers errors in such cases:

typedef deque<int> DeqInt;
typedef DeqInt::iterator Iter;
typedef DeqInt::const_iterator ConstIter;

Iter I;
ConstIter CI;
...

if (I==CI) // requires operator=(iterator) but not a operator=(const_iterator)
    // blahblah|

// To solve it:
if (CI==I)
    // blahblah
    
// * Meyers Tips 26 *


// * Meyers Tips 27 *

// Use std::advance and std::distance to cast const_iterator to iterator but not const_cast.
// const_cast may work for containers like vector and string but it's not 100% sure about it portability.

    std::vector<int> vecInt(10);
    for (int i = 0; i < 10; ++i)
        vecInt[i] = i+10;

    for (auto n: vecInt)
        std::cout << n << " ";
    std::cout.put('\n');

    typedef std::vector<int>::const_iterator ConstIter;
    typedef std::vector<int>::iterator Iter;
    
    ConstIter ci = vecInt.begin();
    ci+=5;
    std::cout << "const iterator points to : " << *ci << std::endl;

    Iter i = vecInt.begin(); // has to point to the same location as ci
    std::advance(i, std::distance<ConstIter>(i,ci)); // move i iterator on the value returned by distance
    std::cout << "iterator points to : " << *ci << std::endl;

// * Meyers Tips 27 *

// * Meyers Tips 28 *

// Lear how to use base() for reverse_iterators
// base() returns a iterator type
// It works properly for pushing element but not for erasing it(iterator returned points to neighbour element)

    std::vector<int> VecInt;
    VecInt.reserve(5);

    for (int i = 1; i <= 5; ++i)
        VecInt.push_back(i);

    for (auto n : VecInt)
        std::cout << n << " ";

    typedef vector<int>::reverse_iterator RevIter;
    RevIter ri = VecInt.rbegin();
//  std::advance(ri , 3);
    ri = std::find(VecInt.rbegin(), VecInt.rend(), 3);

    std::cout << "Reverse iterator point to 3: " << *ri << std::endl;
    std::vector<int>::iterator i(ri.base());
    std::cout << "Normal iterator by base(): " << *i << std::endl;

    VecInt.erase(--ri.base());

    // after deleting
    for (auto n : VecInt)
        std::cout << n << " ";

// * Meyers Tips 28 *

// * Meyers Tips 29 *
// To copy a text from the file to std::string better to use std::istreambuf_iterator<char> instead of std::istream_iterator<char>.
// std::istream_iterator<char> reads every word from buffer by << operator that causes a lot of operations to execute.
// However std::istreambuf_iterator<char> directly reads a word from buffer like: inputFile.rdbuf()->sgetc()

    std::ifstream inputFile("text.txt");
    std::string file((std::istream_iterator<char>(inputFile)),
                     std::istream_iterator<char>());
    std::cout << file << endl;
    
    std::ifstream inputFile("text.txt");
    std::string file((std::istreambuf_iterator<char>(inputFile)),
                     std::istreambuf_iterator<char>());
    std::cout << file << endl;

// * Meyers Tips 29 *

// * Meyers Tips 30 *
// Keep an eye of fit of interval of container where the data has will be copied
// Use inserter(inserter, back_inserter, front_inserter) functions to create a new elemet of container
    const int cSize = 5;
    std::vector<int> values;
    values.reserve(cSize);
    for (int i = 0; i < cSize; ++i)
        values.push_back(i);

    std::vector<int> results;
    results.reserve(cSize);

    std::transform(values.begin(), values.end(),
                   back_inserter(results),
                   transf);

    std::copy(results.begin(), results.end(), std::ostream_iterator<int>(std::cout, " "));
    
// Example with correct saving the size of results container

    const int cSize = 5;
    std::vector<int> values;
    values.reserve(cSize);
    for (int i = 0; i < cSize; ++i)
        values.push_back(i);

    std::vector<int> results;
    results.push_back(7);
    results.push_back(9); // now size of results is 2

    results.reserve(results.size() + values.size()); // 7

    std::transform(values.begin(), values.end(),
                   inserter(results, results.end()), // back_inserter
                   transf);

    std::copy(results.begin(), results.end(), std::ostream_iterator<int>(std::cout, " ")); // 7 9 2 3 4 5 6
    
// * Meyers Tips 30 *

// * Meyers Tips 31 *
    // Don't forget about variations of sort algorithms

    // Complexity in ascending order:
    // partition < stable_partition < nth_element < partial_sort < sort < stable_sort

    // Full sorting of vector/string/deque/array - sort/stable_sort
    std::vector<int> VecInt = {4, 5, 1, 16, 7, 90, 73};
    std::cout << " Before sorting: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));
    std::sort(VecInt.begin(), VecInt.end()); // also comparator as argument
    std::cout << "\n After sort: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    // Selection n first items form the vector/string/deque/array - partial_sort

    VecInt.clear();
    VecInt = { 4, 5, 1, 16, 7, 90, 73, 666, 78, 12};
    std::cout << "\n Before sorting: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    std::partial_sort(VecInt.begin(), VecInt.begin()+6, VecInt.end()); // also comparator as argument
    std::cout << "\n After sort: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    // Indentification first n elements from the vector/string/deque/array - nth_element

    // std::nth_element() is an STL algorithm which rearranges the list in such a way such that
    // the element at the nth position is the one which should be at that position if we sort the list.
    // We use it if we don't need strong order of elements.

    VecInt.clear();
    VecInt = { 4, 5, 1, 16, 7, 90, 73, 666, 78, 12};
    std::cout << "\n Before indentification: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    std::cout << "fd" << *(VecInt.begin()+2);
    std::nth_element(VecInt.begin(), VecInt.begin()+2, VecInt.end());
    std::cout << "\n After indentification: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    // Separation elements from the sequence container to satisfied and dissatisfied to specific criterion - partition/stable_partition

    std::cout << "\n Before separation: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    // Move to the beginning all elements that are divided entirely into 2(even numbers)
    std::partition(VecInt.begin(),VecInt.end(), [](int i){ return ((i % 2) == 0);});
    std::cout << "\n After separation: " << std::endl;
    std::copy(VecInt.begin(), VecInt.end(), std::ostream_iterator<int>(std::cout, " "));

    // list container: partition/stable_partition; instead of sort/stable_sort use list::sort.
    ...

    // Need to imitate partial_sort and nth_element(copy to vector/string/deque/array etc.)
// * Meyers Tips 31 *

// * Meyers Tips 32 *
    // std::remove don't delete elements from the container!
    VecInt.clear();
    VecInt = {1, 2, 4, 99, 5, 6, 99, 7, 8, 99};
    // 1 2 4 99 5 6 99 7 8 99
    // bg                  end
    // after
    std::remove(VecInt.begin(),VecInt.end(), 99);
    std::cout << "\n size after remove: " << VecInt.size() << std::endl;
    // 1 2 4 5 6 7 8 ? ? ? - possibly 1 2 4 5 6 7 8 7, 8, 99
    // bg          end
    // So, std::remove itratively moves elements with replacing element that need to be removed

    // To fix this use erase remove:
    VecInt.clear();
    VecInt = {1, 2, 4, 99, 5, 6, 99, 7, 8, 99};
    VecInt.erase(std::remove(VecInt.begin(),VecInt.end(), 99),VecInt.end());
    std::cout << "\n size after erase remove: " << VecInt.size() << std::endl;
    
    //The same rule is concerns remove_if and unique
// * Meyers Tips 32 *

// * Meyers Tips 33 *
// Be careful with using remove algorithms with 

bool suitableValue(const std::pair<int, std::string> & spair)
{
    return (spair.first < 3);
}

struct Man
{
    Man() {}
    Man(int passport_,std::string name_, std::string surname_) : passport(passport_), name(name_), surname(surname_) {}

    int passport;
    std::string name;
    std::string surname;
};

void delAndNullifyWithIntPredicae(Man* & value)
{
    if (value->passport < 1)
    {
        delete value;
        value = 0;
    }
}

int main()
{

    std::vector<Man*> vec;
    vec.push_back(new Man(0, "Garry", "Southgate"));
    vec.push_back(new Man(1, "Phil", "Southgate"));
    vec.push_back(new Man(2, "Mendy", "Gil"));
    vec.push_back(new Man(3, "Sam", "Fane"));

    std::for_each(vec.begin(), vec.end(), delAndNullifyWithIntPredicae);
    vec.erase(std::remove(vec.begin(), vec.end(), static_cast<Man*>(0)), vec.end());

    return 0;
}

// If we try to use erase remove/remove_if memory leak is possible due to missing iterator pointers(see Tip 32)
// So, romove and remove_if is not recomended to use with containers that containes pointers to dynamically allocated memory.

// * Meyers Tips 33 *

// * Meyers Tips 34 *

Remember algorithms that requires sorted interval as arguments.
List of algorithms that working only with sorted intervals:

binary_search
lower_bound
upper_bound
equal_range
// all above wirking using binary search algorithms. iterators has to be random access type to provide logarithmic complexity else linear.
set_union
set_intersection
set_difference
set_symmetric_difference
// all above has linear complexity
merge
inplace_merge
// all above has linear complexity. They provides merging two sorted ranges 
includes
// linear complexity above. It checks if all objects from one interval is included to another


Algorithms that can work with non-sorted intervals but slowly(if interval is not sorted algorithm cannot delete some unique objects)

unique
unique_copy

Example with uncorrect using algorithm binary search

std::vector<int> vec;
...
std::sort(vec.begin(), vec.end(), greather<int>()); // descending
...
bool b = std::binary_search(vec.begin(), vec.end(), 5); // bad, it supposed that interval is sorted in ascending order
bool b = std::binary_search(vec.begin(), vec.end(), 5, greather<int>()); // good

// * Meyers Tips 34 *

// * Meyers Tips 35 *
Realization of simple comparison of char and string objects wihout taking into account uppercase.

int ciCharCompare(char c1, char c2)
{
    int lc1 = std::tolower(static_cast<unsigned char>(c1));
    int lc2 = std::tolower(static_cast<unsigned char>(c2));

    if (lc1 < lc2) return -1;
    if (lc1 > lc2) return 1;
    return 0;
}

int ciStringCompareImpl(const std::string &str1, const std::string &str2)
{
    typedef std::pair<std::string::const_iterator,
            std::string::const_iterator> PSCI;

    PSCI iter_pair = std::mismatch(str1.begin(), str1.end(),
                                   str2.begin(),
                                   not2(ptr_fun(ciCharCompare)));

    if (iter_pair.first == str1.end())
    {
        if (iter_pair.second == str2.end())
            return 0;
        return -1;
    }
    return ciCharCompare(*iter_pair.first, *iter_pair.second);
}

int ciStringCompare(const std::string &str1, const std::string &str2)
{
    if (str2.size() <= str1.size())
        return ciStringCompareImpl(str1, str2);
    return -ciStringCompareImpl(str1, str2);
}

bool ciCharLess(char c1, char c2)
{
    return std::tolower(static_cast<unsigned char>(c1)) <
            std::tolower(static_cast<unsigned char>(c2));
}

bool ciStringCompare(const std::string &str1, const std::string &str2)
{
    return std::lexicographical_compare(str1.begin(), str1.end(),
                                        str2.begin(), str2.end(),
                                        ciCharLess);
}

// * Meyers Tips 35 *

// * Meyers Tips 36 *
Correct realization of copy_if function.
It is available in C++11

template <typename InputIterator,
          typename OutputIterator,
          typename Predicate>
OutputIterator copy_if(InputIterator begin, InputIterator end, OutputIterator destBegin, Predicate p)
{
    while (begin != end)
    {
        if (p(*begin))
            *destBegin++ = *begin;
        ++begin;
    }
    return destBegin;
}

// * Meyers Tips 36 *

// * Meyers Tips 37 *

// Use accumulate for difficult accumulation/algorithms

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <ostream>
#include <iterator>
#include <numeric>

std::string::size_type lenghtStringSum(std::string::size_type sumSoFar, const std::string &str)
{
   return sumSoFar + str.size();
}

struct Point
{
    Point(double ppX, double ppY) : pX(ppX), pY(ppY) {}

    int pX;
    int pY;
private:
    friend std::ostream & operator<<(std::ostream &os, const Point& p)
    {
        return os << " pX: " << p.pX << " pY: " << p.pY;
    }
};

class PointAverage : public std::binary_function<Point, Point, Point>
{
public:
    PointAverage() : xSum(0), ySum(0), pointCount(0) {}

    const Point operator()(const Point &/*avgSoFar*/, const Point & p)
    {
        ++pointCount;
        xSum += p.pX;
        ySum += p.pY;

        return Point(xSum/pointCount, ySum/pointCount);
    }
private:
    int xSum;
    int ySum;
    int pointCount;
};


int main()
{
    std::vector<int> vi;
    vi.push_back(5);
    vi.push_back(6);
    vi.push_back(2);

    std::vector<double> vd;
    vd.push_back(4.9);
    vd.push_back(6.1);
    vd.push_back(2.0);

    int intSum = std::accumulate(vi.begin(), vi.end(), 0);
    int doubleSum = std::accumulate(vd.begin(), vd.end(), 0.0);

    std::vector<std::string> vs;
    vs.push_back("name");
    vs.push_back("surname");
    vs.push_back("date");

    int stringLenghtSum = std::accumulate(vs.begin(),vs.end(), 0, lenghtStringSum);

    std::vector<float> vf;
    vd.push_back(4.9);
    vd.push_back(6.1);
    vd.push_back(2.0);
    int floatMultiply = std::accumulate(vf.begin(), vf.end(), 1.0, std::multiplies<float>());

    std::vector<Point> vp;
    vp.push_back(Point(0,0));
    vp.push_back(Point(5,5));
    vp.push_back(Point(10,10));

    Point averagePoint = std::accumulate(vp.begin(), vp.end(), Point(0,0), PointAverage());
    std::cout << "Average Point: " << averagePoint;
    
// Also use for_each:

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <ostream>
#include <iterator>
#include <numeric>

std::string::size_type lenghtStringSum(std::string::size_type sumSoFar, const std::string &str)
{
   return sumSoFar + str.size();
}

struct Point
{
    Point(double ppX, double ppY) : pX(ppX), pY(ppY) {}

    int pX;
    int pY;
private:
    friend std::ostream & operator<<(std::ostream &os, const Point& p)
    {
        return os << " pX: " << p.pX << " pY: " << p.pY;
    }
};

class PointAverage : public std::unary_function<Point,void>
{
public:
    PointAverage() : xSum(0), ySum(0), pointCount(0) {}

    void operator()(const Point & p)
    {
        ++pointCount;
        xSum += p.pX;
        ySum += p.pY;
    }

    Point results() const
    {
        return Point(xSum/pointCount, ySum/pointCount);
    }
private:
    int xSum;
    int ySum;
    int pointCount;
};


int main()
{
    std::vector<Point> vp;
    vp.push_back(Point(0,0));
    vp.push_back(Point(5,5));
    vp.push_back(Point(10,10));

    Point averagePoint = std::for_each(vp.begin(), vp.end(), PointAverage()).results();
    std::cout << "Average Point: " << averagePoint;
}


}


// * Meyers Tips 37 *

// Meyers Tip 38
Project Functor classes assuming that they have to be passed by value

template<typename T>
class BPFC : public std::unary_function<T, void>
{
public:
    // ...
    // this virtual function has a problem of loose the data of derived classes of T
    // as we know Functor classes is passed as values and copies
    virtual void operator()(const T& val) const;
private:
    Widget w;
    int i;
    // ... big polymorphic functor class, contains big size of data. There is the problem when pass it bu value
    // - size of instance is really big
};

// To avoid such problems:
// create in implementation of such class and call it from
template<typename T>
class BPFCImpl : public std::unary_function<T, void>
{
private:
    Widget w;
    int i;

    virtual ~BPFCImpl();
    virtual void operator()(const T& val) const;

    friend class BPFC<T>;
};


// compact and monomorphic class
template<typename T>
class BPFC : public std::unary_function<T, void>
{
public:
    void operator()(const T& val) const { impl->operator()(val); }
private:
    BPFCImpl *impl; // need to provide copy constructor! Be careful and use std::shared_pointer
};

// Meyers Tip 38

// * Meyets Tips 40
What is ptr_fun?
ptr_fun is deprecated in c++11

#include <string>
#include <iostream>
#include <algorithm>
#include <functional>
 
bool isvowel(char c)
{
    return std::string("aeoiuAEIOU").find(c) != std::string::npos;
}
 
int main()
{
    std::string s = "Hello, world!";
    std::copy_if(s.begin(), s.end(), std::ostreambuf_iterator<char>(std::cout),
                 std::not1(std::ptr_fun(isvowel)));
// C++11 alternatives: 
//               std::not1(std::cref(isvowel)));
//               std::not1(std::function<bool(char)>(isvowel)));
// C++17 alternative: 
//               std::not_fn(isvowel));
 
}

// * Meyers Tips 40

// * Meyers Tips 41
ptr_fun, mem_fun, mem_fun_ret
Evereone is deprecated in C++11

Example:

#include <iostream>
#include <vector>
#include <functional>

using namespace std;

class Shape {
public:
  virtual void draw(){cout << "Drawing a shape\n"; };
};


class Triangle : public Shape {
  void draw() {cout << "Drawing a triangle\n"; }
};

class Square : public Shape {
  void draw() {cout << "Drawing a square\n"; }
};

int main() {
  vector<Shape*> shapes;
  Triangle t1,t2;
  Square s1, s2;
  shapes.push_back(&t1);
  shapes.push_back(&s1);
  shapes.push_back(&t2);
  shapes.push_back(&s2);
 
  for_each( shapes.begin(),  shapes.end(), mem_fun(&Shape::draw)); 
}

link:
http://www-h.eng.cam.ac.uk/help/tpl/languages/C++/mem_fun.html


// * Meyers Tips 41

// * Meyers Tips 42
Take care of less<T> to mean operator< for correct compare.

1. FIRST PROBLEM:

#include <iostream>
#include <set>

class Employee
{
public:
    Employee(int age, int experience) : m_age(age), m_experience(experience) {}

    int age() const {return m_age;}
    int experience() const {return m_experience;}
private:
    int m_age;
    int m_experience;
};

int main()
{
    Employee e1(20, 2);
    Employee e2(22, 2);
    Employee e3(18, 1);

    std::multiset<Employee> setEmployee;
    setEmployee.insert(e1);
    setEmployee.insert(e2);
    setEmployee.insert(e3);

    for (std::multiset<Employee>::const_iterator i = setEmployee.begin(); i != setEmployee.end(); ++i)
    {
        std::cout << "Employee age " << i->age(); // <= Here is the problem! operator<() has to be declared! error during compilation. 
    }
    return 0;
    
SOLVING PROBLEM 1:

class Employee
{
public:
    Employee(int age, int experience) : m_age(age), m_experience(experience) {}

    int age() const {return m_age;}
    int experience() const {return m_experience;}

    bool operator<(const Employee &e) const { return this->age() < e.age(); }

private:
    int m_age;
    int m_experience;
};

int main()
{
    Employee e1(20, 2);
    Employee e2(22, 3);
    Employee e3(18, 1);

    std::multiset<Employee> setEmployee;
    setEmployee.insert(e1);
    setEmployee.insert(e2);
    setEmployee.insert(e3);

    for (std::multiset<Employee>::const_iterator i = setEmployee.begin(); i != setEmployee.end(); ++i)
    {
        std::cout << "Employee age " << i->age() << " Employee experience " << i->experience() << std::endl;
    }
    return 0;
}

Result: 
Employee age 18 Employee experience 1
Employee age 20 Employee experience 2
Employee age 22 Employee experience 3


2. PROBLEM: But if we want not to use operator< but sort by experience? We can use custom functor in this case:

class Employee
{
public:
    Employee(int age, int experience) : m_age(age), m_experience(experience) {}

    int age() const {return m_age;}
    int experience() const {return m_experience;}

    bool operator<(const Employee &e) const { return this->age() < e.age(); }

private:
    int m_age;
    int m_experience;
};

struct EmployeeComparator : std::binary_function<Employee, Employee, bool>
{
    bool operator()(const Employee &leftE, const Employee &rightE) const { return leftE.experience() < rightE.experience(); }
};

int main()
{
    Employee e1(20, 2);
    Employee e2(22, 1);
    Employee e3(18, 3);

    std::multiset<Employee, EmployeeComparator> setEmployee;
    setEmployee.insert(e1);
    setEmployee.insert(e2);
    setEmployee.insert(e3);

    for (std::multiset<Employee>::const_iterator i = setEmployee.begin(); i != setEmployee.end(); ++i)
    {
        std::cout << "Employee age " << i->age() << " Employee experience " << i->experience() << std::endl;
    }
    return 0;
}

Result:

Employee age 22 Employee experience 1
Employee age 20 Employee experience 2
Employee age 18 Employee experience 3


// * Meyers Tips 42